import requests
import codecs
from time import gmtime, strftime

Feeds = [
    {'URL'      : 'http://www.smh.com.au/rssheadlines/business/article/rss.xml',
     'FileName' : 'smh-business'},
    
    {'URL'      : 'https://theconversation.com/au/business/articles.atom',
     'FileName' : 'conversation-business-economy'},
    
    {'URL'      : 'http://abs.gov.au/AUSSTATS/wmdata.nsf/activerss/abs_rss/$File/abs_rss.xml',
    'FileName'  : 'abs-product'},
    
    {'URL'      : 'http://abs.gov.au/AUSSTATS/wmdata.nsf/activerss/mediaReleases_rss/$File/mediaReleases_rss.xml',
    'FileName'  : 'abs-media'},
    
    {'URL'      : 'http://feeds.reuters.com/Reuters/worldNews',
    'FileName'  : 'reuters-world'},
    
    {'URL'      : 'http://feeds.reuters.com/reuters/businessNews',
    'FileName'  : 'reuters-business'},
    
    {'URL'      : 'https://www.lowyinstitute.org/the-interpreter/rss.xml',
    'FileName'  : 'lowy-interpreter'},
    
]

for Feed in Feeds:
    print strftime("%Y-%m-%d %H:%M:%S", gmtime()), 'The machine is retrieving data from %s.' % (Feed['URL'])
    CurrentFeed = requests.get(Feed['URL'])
    
    print strftime("%Y-%m-%d %H:%M:%S", gmtime()), 'The machine is writing data to %s.' % (Feed['FileName'])
    FeedFile = codecs.open('feeds/' + Feed['FileName'], 'w', 'utf-8')
    FeedFile.write(CurrentFeed.text)
    print strftime("%Y-%m-%d %H:%M:%S", gmtime()), 'The machine has completed another full instruction set.\n'
    