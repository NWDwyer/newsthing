import codecs
from time import gmtime, strftime
from datetime import datetime 

StoryCardFilePath = 'story-cards'
StoryCards = ""

print strftime("%Y-%m-%d %H:%M:%S:", gmtime()) + str(datetime.now().microsecond), "The machine is backcharging from the data store."

StoryCardFile = codecs.open(StoryCardFilePath, 'r', 'utf-8')
Template = codecs.open("template_index.html", 'r', 'utf-8')
Index = codecs.open("index.html", 'w', 'utf-8')

print strftime("%Y-%m-%d %H:%M:%S:", gmtime()) + str(datetime.now().microsecond), "When this plays, the machine is entering the Display phase."

StoryCards = StoryCardFile.read()

TemplateString = Template.read()
Index.write(TemplateString % StoryCards)