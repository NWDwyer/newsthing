import os
import codecs
import feedparser
import random
from time import gmtime, strftime
from datetime import datetime
from operator import itemgetter


def FormatDate(SourceDate):
    
    return strftime('%c', SourceDate)


def RandomColour(Feed):
    
    random.seed(Feed) # Is it possible that this will make colours consistent across sessions!?    
    ColourComponents = [0, 0, 0]
    Colour = ""
    
    for Component in ColourComponents:
        Component = random.randint(0, 255)
        Colour += str(Component) + ", "
    
    # Remove the trailing ", "
    Colour = Colour[:-2]
    
    return Colour


### Setting things up ###

FeedFiles = []
Feeds = []

# Iterate over the feeds directory
for FeedFile in os.listdir(r'feeds/'):
    Feeds.append(feedparser.parse(r'feeds/' + FeedFile))

StoryCardTemplate = {
    'AnchorTagLine'     : '<a class="storycard" style="border-color: rgb(%s);" href="%s">\n',
    'FeedTitle'         : '<em>%s</em><br>\n',
    'StoryHeadline'     : '<span class="headline"><b>%s</b></span><br>\n',
    'StoryDescription'  : '%s<br>\n',
    'FormattedDate'     : '<span class="date"><em>%s</em></span></a><br>\n'
}

StoryCards = []
StoryCardString = ''


### Parsing the feeds ###

for Feed in Feeds:
    
    FeedColour = RandomColour(Feed.feed.title)
    
    # Print this in utf-8 so that we don't break the console when we write our log file
    DebugString = strftime("%Y-%m-%d %H:%M:%S:", gmtime()) + str(datetime.now().microsecond) + ' The machine is analysing data from %s.' % (Feed.feed.title).encode('utf-8')
    print DebugString

    for Entry in Feed.entries:
        
        StoryCard = {
            'AnchorTagLine'     : StoryCardTemplate['AnchorTagLine'] % (FeedColour, Entry.link),
            'FeedTitle'         : StoryCardTemplate['FeedTitle'] % (Feed.feed.title),
            'StoryHeadline'     : StoryCardTemplate['StoryHeadline'] % (Entry.title),
            'StoryDescription'  : '',
            'PublishedDate'     : '', # This tuple is a top-secret variable we must never show to the user!
            'FormattedDate'     : ''
        }

        # This is so we don't show The Conversation's "content" field, which feedparser interprets as being a "description"
        if 'description' in Entry and 'content' not in Entry:
            StoryCard['StoryDescription'] = StoryCardTemplate['StoryDescription'] % (Entry.description[:100] + '...')

        if 'published' in Entry:
            StoryCard['PublishedDate'] = (Entry.published_parsed) # Shhhhh!
        else:
            # TODO: figure out how to get the date of the last ABS release maybe?
            StoryCard['PublishedDate'] = (gmtime())
        
        StoryCard['FormattedDate'] = StoryCardTemplate['FormattedDate'] % FormatDate(StoryCard['PublishedDate'])

        StoryCards.append(StoryCard)


### Sorting and stringifying for export ###

# Sort the dictionary items in descending order of published date
StoryCards.sort(key=itemgetter('PublishedDate'), reverse=True)

# Convert the StoryCards values into a big string

# Can we use sorted() on the dictionary keys instead of creating this list manually?
StoryCardKeys = ['AnchorTagLine', 'FeedTitle', 'StoryHeadline', 'StoryDescription', 'FormattedDate']

for StoryCard in StoryCards:
    
    for Key in StoryCardKeys:
        StoryCardString += StoryCard[Key]
    

print strftime("%Y-%m-%d %H:%M:%S:", gmtime()) + str(datetime.now().microsecond), "The machine is charging the data store."
StoryCardFile = codecs.open('story-cards', 'w', 'utf-8')
StoryCardFile.write(StoryCardString)

print strftime("%Y-%m-%d %H:%M:%S:", gmtime()) + str(datetime.now().microsecond), 'The machine has completed another full instruction set.\n'