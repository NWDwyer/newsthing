Debt
    - parse.py
        * 20170617 - Line 73: In order to exclude The Conversation's "content" field, we just ignore "description" entirely if there's also "content" present. This will prevent us from getting the description from a feed if there is both "description" and "content".
        
        * 20171619 - Line 78: In order to give the ABS feeds a date we can use to sort the feeds, we just use the current date & time when the script is run. This means that the ABS will always be considered the "most recent" feed. I think the solution is to make the publication date "the last time it was 11:30 on a weekday" but I don't see how to begin that process without saying "if this feed is from the ABS".
        
        * 20170625 - Line 75: Reuters feeds were throwing rogue anchor tags into their descriptions, making the layout go crazy because story cards are also anchor tags. It may be possible to make the CSS adapt to this problem (or to sanitise the feed data by removing anchor tags) but instead I am now only taking the first 100 characters of all descriptions. Yuck.


Happy accidents
    - parse.py
        * Line 12: Just for fun, I decided to seed the random colour generator with the title of the feed in question. A happy by-product of this is that feeds appear to retain their border colours, even when the index is regenerated!