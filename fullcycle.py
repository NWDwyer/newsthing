import random
from time import gmtime, strftime
from datetime import datetime 

StartingDialogueChoices = ["End of sleep phase.", "This playback indicates the machine is waking up.", "Activation.", "Well, err, the machine should enter active state now."]
print strftime("%Y-%m-%d %H:%M:%S:", gmtime()), random.choice(StartingDialogueChoices)

import download

import parse

import display

print "\n" + strftime("%Y-%m-%d %H:%M:%S", gmtime()) + str(datetime.now().microsecond), "The machine is running at low speed."